# Production Planning

Production planning problem used in the ACAI'14 programming competition.

Created by: Alessio Bonfietti, Michele Lombardi, Pierre Schaus 

## Competition

Check out the :boxing_glove: [the competition scoreboard on wiki](https://gitlab.com/agh-courses/2020-2021/artificial-intelligence/projects/production-planning/-/wikis/Scoreboard/). :boxing_glove:
## Instructions

1. Fork this project into a **private** group:
- if you don't have a private group, create one - give it a name of your team, cool names are cool
- this requirement is caused by a GitLab bug: https://gitlab.com/gitlab-org/gitlab/-/issues/214446
2. Add @mateusz.slazynski as the new project's member (role: ``maintainer``)
3. Change project permissions from ``internal`` to ``private``
4. Check the ``handout.pdf`` for the instructions.
5. Add to the output a line of form ``obj = <objective>`` where ``objective`` is the value of optimized function (as in the ``solve maximize <objective>``). 
6. Solve the problem!
7. Automated tests will be run periodically to check quality of your model. The results will be available on top of the ``README.md``. Details will be available in the ``test.log`` file. Of course you can run the tests by yourself - just run the ``test.sh`` script (with decent versions of ``bash`` and ``MiniZinc`` on Unix). More tests may be added in the future.
8. If done before the deadline, contact @mateusz.slazynski via Slack, so he can check it earlier.
9. To take part in the competition create file ``competition.sol`` containing solution for the ``big.dzn`` instance.

## Problem Details

In case handout was not informative enough: our task is to schedule production on the single assembly line. Every item has a specified type and deadline. Before producing an item, assembly line has to be configured specifically to the item's type. Also, every item has to be produced before its deadline.

We minimize two costs: 

- storage cost - we pay for every day the complete item is kept at the factory - so we want to produce items as late as possible.
- change cost - reconfiguring the assembly line is expensive and every configuration change costs value specified in a matrix (i.e. change from producing type 1 to type 2 costs such and such amount of money)

``data`` folder contains data files - there are two instances: 
- ``small`` just to check the model,
- ``big`` for the competition.

## Output

Below is an example output for the small instance, where ``-1`` means that the assembly line was idle at the time. ``0`` is item of type ``0``, etc. Notice that in the data file types start from ``1``, so you have to adjust them to the output requirements.

```
solution = [1, -1, -1, 9, 0, 6, 5, 7, 4, 10, 3, 11, 2, -1, 8]; 
obj = 1294;
```

